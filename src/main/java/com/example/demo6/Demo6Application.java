package com.example.demo6;

import com.example.demo6.component.employee;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;
import java.util.Properties;

@SpringBootApplication
public class Demo6Application {

	public static void main(String[] args) {
		sessionFactory = getSessionFactory();
		Session sessionObj = sessionFactory.openSession();
		saveRecord(sessionObj);
		//display(sessionObj);
		//update(sessionObj);

		delete(sessionObj);


		//SpringApplication.run(Demo6Application.class, args);
	}


	private static SessionFactory sessionFactory;

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try {
				Configuration configuration = new Configuration();

				// Hibernate settings equivalent to hibernate.cfg.xml's properties
				Properties settings = new Properties();
				settings.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
				settings.put(Environment.URL, "jdbc:mysql://localhost:3306/java");
				settings.put(Environment.USER, "root");
				settings.put(Environment.PASS, "root");
				settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");


				settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");

				settings.put(Environment.HBM2DDL_AUTO, "create-drop");

				configuration.setProperties(settings);
				settings.put(Environment.SHOW_SQL, "true");
				configuration.addAnnotatedClass(employee.class);

				ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
						.applySettings(configuration.getProperties()).build();

				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sessionFactory;
	}

	private static void saveRecord(Session sessionObj) {
		employee e = new employee();
		e.setEid(2);
		e.setEname("varun");
		e.setEsal(4000);
		e.setEdept("finance");
		sessionObj.beginTransaction();
		sessionObj.save(e);
		sessionObj.getTransaction().commit();
		System.out.println("Record added");

	}

	/*public static void display(Session sessionObj) {
		Query queryObj = sessionObj.createQuery("From employee");
		List<employee> listObj = queryObj.list();
		for (employee iterate : listObj) {
			System.out.println(iterate.getEid() + " " + iterate.getEname() + " " + iterate.getEsal() + " " + iterate.getEdept());
		}*/
	/*	public static  void update(Session sessionObj){
			int eid=2;
			employee employeeObj=(employee) sessionObj.get(employee.class,eid);
			employeeObj.setEname("kumar");
			employeeObj.setEdept("customer support");
			sessionObj.beginTransaction();
			sessionObj.saveOrUpdate(employeeObj);
			sessionObj.getTransaction().commit();
			System.out.println("update records");

		}*/
		public static void delete(Session sessionObj){
			int eid=2;
			employee employeeObj = (employee) sessionObj.get(employee.class, eid);
			//sessionObj.get(employee.class, eid);
			sessionObj.beginTransaction();
			sessionObj.delete(employeeObj);
			sessionObj.getTransaction().commit();
			System.out.println("delete records");
		}
	}






