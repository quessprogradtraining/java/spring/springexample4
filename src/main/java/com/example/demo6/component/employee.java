package com.example.demo6.component;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class employee {
    @Id

    private String ename;
    private int eid;
    private int esal;
    private  String edept;

    public employee() {
    }

    public employee(String ename, int eid, int esal, String edept) {
        this.ename = ename;
        this.eid = eid;
        this.esal = esal;
        this.edept = edept;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public int getEid() {
        return eid;
    }

    public void setEid(int eid) {
        this.eid = eid;
    }

    public int getEsal() {
        return esal;
    }

    public void setEsal(int esal) {
        this.esal = esal;
    }

    public String getEdept() {
        return edept;
    }

    public void setEdept(String edept) {
        this.edept = edept;
    }
}
